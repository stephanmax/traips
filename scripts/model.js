import Geohash from 'https://cdn.jsdelivr.net/npm/latlon-geohash@2.0.0';
import { roundAccurately } from './util.js';

export default class Model {
  constructor(listener) {
    this.isLoading = false;
    this.listener = listener;
    this.places = new Map();
    const URLQueryString = window.location.search;
    if (URLQueryString.includes('?')) {
      this.isLoading = true;
      const urlParams = new URLSearchParams(window.location.search);
      for (const [id, note] of urlParams) {
        const {lat, lon} = Geohash.decode(id);
        this.create({
          id,
          note,
          lat: roundAccurately(lat, 5),
          lng: roundAccurately(lon, 5)
        });
      }
      // Loading mechanism is needed because we neither want to save (we just loading the state of the model!)
      // nor do we want focus every newly created place-note
      this.isLoading = false;
      this.listener.onLoadComplete();
    }
  }

  save() {
    const params = new URLSearchParams();
    // Todo: Do cleanup and validation earlier? When working on the model?
    [...this.places].forEach(([key, {id, note}]) => {
      params.append(id, note.trim());
    });
    history.pushState({}, '', `?${params.toString()}`);
  }

  create(place) {
    if (!place.id) {
      place.id = Geohash.encode(place.lat, place.lng, 10);
    }
    this.places.set(place.id, {
      ...place
    });
    this.listener.onCreate(place, this.isLoading);
    this.listener.onCountChange(this.places.size);

    if (!this.isLoading) {
      this.save();
    }
  }

  update(id, placeData) {
    // Todo: Better guard whether there are any actual updates
    if (this.places.get(id).note === placeData.note) {
      return;
    }
    this.places.set(id, {
      ...this.places.get(id),
      ...placeData
    });
    this.listener.onUpdate(this.places.get(id));
    this.save();
  }

  delete(id) {
    this.places.delete(id);
    this.listener.onDelete(id);
    this.listener.onCountChange(this.places.size);
    this.save();
  }
}
