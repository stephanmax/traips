import Model from '../scripts/model.js';
import { roundAccurately } from './util.js';

const setDocHeight = () => {
  const doc = document.documentElement;
  doc.style.setProperty('--doc-height', `${window.innerHeight}px`);
};
window.addEventListener('resize', setDocHeight);
setDocHeight();

const map = new L.Map('p-map', {
  center: new L.LatLng(51.34, 12.376),
  doubleClickZoom: false,
  maxZoom: 18,
  minZoom: 3,
  zoom: 16
});
const tileLayer = L.tileLayer("http://{s}tile.stamen.com/toner-lite/{z}/{x}/{y}.png", {
  subdomains: ['', 'a.', 'b.', 'c.', 'd.'],
  attribution: 'TODO'
});
tileLayer.addTo(map);
const markerGroup = L.layerGroup().addTo(map);
const markers = new Map();
map.on('click', (e) => {
  placeModel.create({
    note: '',
    lat: roundAccurately(e.latlng.lat, 5),
    lng: roundAccurately(e.latlng.lng, 5)
  });
});

const svgIcon = L.divIcon({
  html: `<svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 24 24" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M12 21.7C17.3 17 20 13 20 10a8 8 0 1 0-16 0c0 3 2.7 6.9 8 11.7z"/><circle cx="12" cy="10" r="3"/></svg>`,
  className: "svg-icon",
  iconSize: [32, 32],
  iconAnchor: [16, 32],
});

const placeModel = new Model(new class {
  onCreate(place, isLoaded = false) {
    const placeNoteItem = document.querySelector('#p-list template').content.cloneNode(true).firstElementChild;
    placeNoteItem.id = `p-note-${place.id}`;
    const placeNoteForm = placeNoteItem.firstElementChild;
    placeNoteForm.id = `p-form-${place.id}`;
    placeNoteForm.elements.note.value = place.note;

    placeNoteForm.addEventListener('submit', (e) => {
      e.preventDefault();
      e.target.elements.note.blur();
    });
    placeNoteForm.elements.note.addEventListener('blur', () => {
      placeModel.update(place.id, Object.fromEntries(new FormData(placeNoteForm)));
    });
    placeNoteForm.elements.delete.addEventListener('click', () => {
      placeModel.delete(place.id);
    });
    document.querySelector('#p-list').appendChild(placeNoteItem);

    const marker = L.marker([place.lat, place.lng], {
      // draggable: true,
      icon: svgIcon
    }).on('click', () => {
      window.location.hash = `p-note-${place.id}`;
    });
    markers.set(place.id, marker);
    markerGroup.addLayer(marker);

    // If the place was created manually, focus the input field
    // to allow immediate typing of an accompanying note
    if (!isLoaded) {
      placeNoteForm.elements.note.focus();
    }
  }

  onUpdate(place) {
    const placeNoteForm = document.forms[`p-form-${place.id}`];
    placeNoteForm.elements.note.blur();
  }

  onDelete(id) {
    document.forms[`p-form-${id}`].parentNode.remove();
    markerGroup.removeLayer(markers.get(id));
    markers.delete(id);
  }

  onCountChange(count) {
  }

  onLoadComplete() {
    const bounds = Array.from(markers.values()).map(m => m.getLatLng());
    map.fitBounds(L.latLngBounds(bounds).pad(.1));
  }
});

// document.forms.main.elements.share.addEventListener('click', ({target}) => {
//   target.classList.add('copied');
//   setTimeout(() => {
//     target.classList.remove('copied');
//   }, 1500);
//   navigator.clipboard.writeText(location.protocol + '//' +
//     location.host + location.pathname + location.search);
// });
