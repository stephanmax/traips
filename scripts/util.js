export const roundAccurately = (num, precision = 5) => {
  const numAbs = Math.abs(num);
  const rounded = Number(`${Math.round(`${numAbs}e${precision}`)}e-${precision}`);
  return num < 0 ? -1 * rounded : rounded;
};
